Buildsystem der xmz-platform.

# Vorbereitung

```bash
git submodule init
git submodule update
```

# Start

```bash
cd poky
source ./oe-init-build-env
```

# Arch Buildhost Probleme

Arch wird nicht offiziell als Buildhost unterstützt.

Jedoch gibt es eine funktionierende Anleitung im Arch Wiki mit der Bitbake
gebaut werden kann.

## Probleme mit `file`
* https://lists.yoctoproject.org/g/poky/topic/70908401?p=,,,20,0,0,0::,,,0,0,0,70908401
* https://git.archlinux.org/svntogit/packages.git/commit/trunk?h=packages/file&id=a52fa0a37bad1f4ab67a934a4e0025e70aa1fd0e


Die Lösung für dieses Problem ist ein Downgrade der `file` Version.

```bash
sudo pacman -U file-5.37-5-x86_64.pkg.tar.xz
```

Abschließend wird mit folgendem Eintrag in der Datei `/etc/pacman.conf` dafür
gesorgt das `file` nicht wieder aktualisiert wird.

```bash
# /etc/pacman.conf
IgnorePkg = file
```




